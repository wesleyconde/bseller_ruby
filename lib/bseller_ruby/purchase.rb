module BsellerRuby
  class Purchase < Base
    attr_reader :response

    def self.all(params = {})
      get(path, params)
    end

    private

      def self.path
        'compras/requisicao'
      end
  end
end
