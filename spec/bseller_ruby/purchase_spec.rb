require 'spec_helper'

describe BsellerRuby::Item do
  describe '.all' do
    it 'get purchases' do
      VCR.use_cassette('purchases_list') do
        response = BsellerRuby::Purchase.all({fornecedor: 1, departamento: 2})
        expect(response).to an_instance_of(Array)
      end
    end
  end
end
